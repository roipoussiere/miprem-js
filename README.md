# Miprem - Javascript implementation

The Javascript implementation of [Miprem](https://roipoussiere.frama.io/miprem/).

## Running demo page locally

Since code blocks content of the demo page are fetched from dedicated files, the demo page can not be run by just opening `index.html`.

You must create a web server first, for instance with Python:

```bash
python -m http.server -d public
```

Note that `public/vendor/miprem.js` is a relative symlink to `dist/miprem.js`.

You can then open the demo:

```bash
firefox 127.0.0.1:8000
```

## Building the library

If you work on miprem.js, you must run `build.py` from the miprem-js root folder after your edit:

```bash
./build.py
```

It generates `dist/miprem.js`, which is a merge of:

- the Miprem renderer: `./renderer.js`;
- assets: `./assets.js` (witch declares *DefaultData*, *PollSample* and *Templates* classes), with {{ <class_arg_name> }} occurrences replaced by their corresponding string literal;
- vendors: `./vendor/handlebars.min.js`.
